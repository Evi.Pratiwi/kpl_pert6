package main;
import org.apache.velocity.app.VelocityEngine;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import static spark.Spark.get;
import java.util.HashMap;
import spark.ModelAndView;

public class App {

    private static final Logger log = Logger.getLogger(App.class.getName());

    public static void main(String[] args) throws IOException {
//        log.log(Level.SEVERE, "This is SEVERE Level");
//        log.log(Level.WARNING, "This is WARNING LEVEL");
//        log.log(Level.INFO, "This is INFO LEVEL");

        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);

        get("/log", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
//An array of size 3
            int []a = {1,2,3};
            int index = 3;
            model.put("a" , Arrays.toString(a));
            model.put("index" , index);
            try{
                int test = a[index];
                model.put("test", test);
            } catch (ArrayIndexOutOfBoundsException ex){
                log.log(Level.SEVERE,"Exception Occur", ex);
            }
            String templatePath = "/views/log.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/throwing", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String s = "Saya";
            if(s == null){
                throw new NullPointerException();
            }
            if (s.equals("")){
                return true;
            }
            String first = s.substring(0,1);
            String rest = s.substring(1);
            boolean hasil = first.equals(first.toUpperCase()) &&
                    rest.equals(rest.toLowerCase());
            model.put("hasil", hasil);
            String templatePath = "/views/throwing.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/nullPointer", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String s = null;
            String first = s.substring(0,1);
            String rest = s.substring(1);
            boolean hasil = first.equals(first.toUpperCase()) &&
                    rest.equals(rest.toLowerCase());
            model.put("s",s);
            model.put("first", first);
            model.put("rest", rest);
            model.put("hasil", hasil);
            String templatePath = "/views/nullPointer.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        try {
            division(200, 5);
            division(200, 0); // Divide by zero
        } catch (ArithmeticException e) {
            System.out.println("Divide by zero exception : "
                    + e.getMessage());
        } catch (IOException c){
            System.out.println(c.getMessage());
        }
    }

    public static void division(int totalSum, int totalNumber)
            throws ArithmeticException, IOException {
        int average = totalSum / totalNumber;
// Additional operations that may throw IOException...
        System.out.println("Average: " + average);
    }

}
